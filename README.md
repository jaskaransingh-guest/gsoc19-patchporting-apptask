# gsoc19-patchporting-apptask

My submission for the GSoC 2019 application task of the Debian Patch Porting System.

Instructions to run:

1. Clone the repository
2. cd into the folder
3. Install the dependencies using 
    `pip install -r requirements.txt`
4. For any cve xxx-xxxx, run:
    `scrapy crawl patch_spider -a cve=CVE-xxx-xxxx`
or
    `scrapy crawl patch_spider -a cve=xxx-xxxx`
The patches will be downloaded into your working folder.