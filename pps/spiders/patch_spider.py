# -*- coding: utf-8 -*-
import scrapy
import re
import wget
from scrapy.http import Request

class PatchSpiderSpider(scrapy.Spider):
    name = 'patch_spider'

    #Ensure that cve is in correct format and initialize start_urls
    #currently crawling Debian's security tracker, Gentoo's bug tracker and Ubuntu's bug tracker
    def __init__(self,cve=""):
        if (re.match(r'\d+-\d+',cve)):
            cve = 'CVE-'+cve
        #cve_year = cve.split('-')[1]
        self.start_urls = [
            'https://security-tracker.debian.org/tracker/'+cve,
            'https://bugs.gentoo.org/show_bug.cgi?id='+cve
        ]
        self.patches_found = []

    #override scrapy's default start_requests method
    def start_requests(self):
        for url in self.start_urls:
            if 'security-tracker.debian.org' in url:
                yield Request(url, callback=self.parse_debian_tracker)
            else:
                yield Request(url, callback=self.parse)

    #to parse security-tracker.debian.org/tracker/{cve}
    #Parses the CVE page and looks for any possible patches
    #also parses any links found in the Notes section of the page
    def parse_debian_tracker(self,response):
        notes = response.xpath('//pre/a/@href').extract()
        for note in notes:
            if (re.search(r'[0-9a-fA-F]{40}',note) and re.search(r'commit',note)):
                if (note not in self.patches_found):
                    self.patches_found.append(note)
                    link = self.format_patch_link(note,response.url)
                    self.download_patch(link)
            else:
                yield Request(url=note,callback=self.parse)

    #Parsing for any site/tracker other than debian's security tracker
    def parse(self, response):
        links = response.css('a::attr(href)').extract()
        print(response.url)
        for link in links:
            if (re.search(r'[0-9a-fA-F]{40}',link) and re.search(r'commit',link) and link not in self.patches_found):
                self.patches_found.append(link)
                link = self.format_patch_link(link,response.url)
                self.download_patch(link)

    #format the commit link to a patch link that can be downloaded
    def format_patch_link(self,link,site_url):
        if (re.search(r'^/',link)):
            link = 'https://' + site_url.split('/')[2] + link
        if (re.search(r'github.com/.+?/.+?/commit/[0-9a-fA-F]+',link)):
            link = link + '.patch'
        elif (re.search(r'.*/commit/.*',link)):
            link = link.replace('commit','patch')
        return link

    #download patch using wget
    def download_patch(self,link):
        filename = wget.download(link)

